const AddUser = props => {
    return (
        <form>
            <label htmlFor="username">Username</label>
            <input id="username" type="text" />
            <label htmlFor="age">Age (Years)</label>
            <input id="age" type="number" />
        </form>
    );
};

export default AddUser;